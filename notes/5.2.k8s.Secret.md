## #Secret

>  Secret存储密码、token、密钥等敏感数据  
>  Secret以Volume或环境变量方式使用 

 #Secret类型

-  Opaque :   base64 编码格式的 Secret 
-  kubernetes.io/dockerconfigjson :  私有docker registry认证信息
-  kubernetes.io/service-account-token  : 用来访问 Kubernetes API，由 Kubernetes 自动创建

#### # Opaque Secret

```
#生成secret-demo.yaml文件

echo "#secret-demo.yaml
apiVersion: v1
kind: Secret
metadata:
  name: secret-demo
type: Opaque
data:
  username: $(echo -n "admin" | base64)
  password: $(echo -n "admin123" | base64)
" |tee secret-demo.yaml
```

```
#创建
kubectl create -f secret-demo.yaml

#查看
kubectl get secret
kubectl describe secret secret-demo
kubectl get secret secret-demo -o yaml
```

  #环境变量方式挂载

```
#secret1-pod.yaml

apiVersion: v1
kind: Pod
metadata:
  name: secret1-pod
spec:
  containers:
  - name: secret1
    image: busybox:uclibc
    command: [ "/bin/sh", "-c", "env |egrep 'USERNAME|PASSWORD'" ]
    env:
    - name: USERNAME
      valueFrom:
        secretKeyRef:
          name: secret-demo
          key: username
    - name: PASSWORD
      valueFrom:
        secretKeyRef:
          name: secret-demo
          key: password
  restartPolicy: Never
```

```
#创建
kubectl create -f secret1-pod.yaml
#查看
kubectl logs secret1-pod
```

 #Volume 挂载略(使用与ConfigMap 类似)

#### # kubernetes.io/dockerconfigjson

```
#创建
kubectl create secret docker-registry myregistry \
--docker-server=DOCKER_SERVER \
--docker-username=DOCKER_USER \
--docker-password=DOCKER_PASSWORD \
--docker-email=DOCKER_EMAIL
```

```
#查看
kubectl get secret
kubectl describe secret myregistry
kubectl get secret myregistry -o yaml
```

 # docker registry认证使用

```
#registry-demo.yaml
apiVersion: v1
kind: Pod
metadata:
  name: registry-demo
spec:
  containers:
  - name: demo
    image: 192.168.18.100:5000/test:v1
  imagePullSecrets:
  - name: myregistrykey
```


### #清理demo

```
kubectl delete -f secret1-pod.yaml
kubectl delete -f secret-demo.yaml
kubectl delete -f registry-demo.yaml
kubectl delete secret myregistry
```


> 参考 https://www.qikqiak.com/k8s-book/docs/29.Secret.html

---
>  Blog地址  http://blog.elvin.vip  
>  本文git地址 https://gitee.com/almi/k8s/tree/master/notes  