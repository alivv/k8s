**k8s笔记分享，重在实操**

详细理论，参考其它网站

Blog  http://blog.elvin.vip  
git地址 https://gitee.com/almi/k8s

---

**k8s在线文档**

kubernetes官方git仓库
https://github.com/kubernetes/kubernetes


kubernetes官方文档
https://kubernetes.io/zh/docs/concepts/

Kubernetes中文社区文档
https://www.kubernetes.org.cn/k8s

Kubernetes指南
https://feisky.gitbooks.io/kubernetes/

Docker到Kubernetes进阶
https://www.qikqiak.com/k8s-book

Kubernetes中文指南
https://jimmysong.io/kubernetes-handbook/

